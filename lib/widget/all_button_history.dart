import 'package:flutter/material.dart';
import 'package:uiprojectbo/widget/available_button.dart';
import 'package:uiprojectbo/widget/history_button.dart';
import 'package:uiprojectbo/widget/transfered_button.dart';
import 'package:uiprojectbo/widget/used_button.dart';
import '../utils/size_config.dart';

class AllButtonHistory extends StatelessWidget {
  const AllButtonHistory({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: [
        _historybutton(context),
        _availablebutton(context),
        _usedbutton(context),
        _transferedbutton(context),
      ],
    );
  }
}

///Widget History Button
Widget _historybutton(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(bottom: SizeConfig.vertical(2)),
    child: const HistoryButton(),
  );
}

///Widget Available Button
Widget _availablebutton(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(bottom: SizeConfig.vertical(1)),
    child: const AvailableButton(),
  );
}

///Widget Used Button
Widget _usedbutton(BuildContext context) {
  return Padding(
      padding: EdgeInsets.only(
        bottom: SizeConfig.vertical(1),
      ),
      child: const UsedButton());
}

///Widget Transfered Button
Widget _transferedbutton(BuildContext context) {
  return Padding(
      padding: EdgeInsets.only(
        bottom: SizeConfig.vertical(1),
      ),
      child: const TransferedButton());
}
