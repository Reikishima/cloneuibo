import 'package:flutter/material.dart';

import 'package:uiprojectbo/widget/button_claim.dart';
import 'package:uiprojectbo/widget/list_holder_widget.dart';
import 'package:uiprojectbo/widget/reservation_widget.dart';
import 'package:uiprojectbo/widget/schedule_event.dart';

import '../utils/size_config.dart';

class ContainerEticket extends StatelessWidget {
  const ContainerEticket({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return  Column(
      children: <Widget>[
        _eventschedule(context),
        _reservationdetail(context),
        _buttonclaim(context),
        _listholder(context),
      ],
    );
  }
}

///Widget Event Schedule
Widget _eventschedule(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(top: SizeConfig.vertical(1.5)),
    child: SizedBox(
      width: SizeConfig.horizontal(90),
      height: SizeConfig.vertical(10),
      child: const ScheduleEvent(),
    ),
  );
}

///Widget Reservation Detail
Widget _reservationdetail(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(top: SizeConfig.vertical(1.5)),
    child: SizedBox(
      width: SizeConfig.horizontal(90),
      height: SizeConfig.vertical(20),
      child: const ReservationWidget(),
    ),
  );
}

///Widget Button Claim
Widget _buttonclaim(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(top: SizeConfig.vertical(1.5)),
    child: SizedBox(
      width: SizeConfig.horizontal(85),
      height: SizeConfig.vertical(5),
      child: const ButtonClaim(),
    ),
  );
}

///Widget List Holder
Widget _listholder(BuildContext context) {
  return Container(
    padding: EdgeInsets.only(left: SizeConfig.horizontal(5)),
    height: SizeConfig.vertical(35),
    child: const ListHolderWidget(),
  );
}