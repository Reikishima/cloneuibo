import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:uiprojectbo/Etiket/custom_malika.dart';
import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class AvailableButton extends StatelessWidget {
  const AvailableButton({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
      width: SizeConfig.horizontal(95),
      height: SizeConfig.vertical(17),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(backgroundColor: AppColors.lightGold),
        onPressed: () {
          // Get.to(CategoriesListMallika1());
        },
        child: _mainwrapper1(context),
      ),
    );
  }
}

/// Widget Main Wrapper
Widget _mainwrapper1(BuildContext context) {
  return Container(
    padding: EdgeInsets.only(bottom: SizeConfig.vertical(0.5)),
    width: SizeConfig.horizontal(85),
    height: SizeConfig.vertical(15),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
          child: SizedBox(
            width: SizeConfig.horizontal(95),
            height: SizeConfig.vertical(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _nada1rindu(context),
                _schedule(context),
                _location1(context),
              ],
            ),
          ),
        ),
        _available(context)
      ],
    ),
  );
}

///Widget Nada 1 Rindu
Widget _nada1rindu(BuildContext context) {
  return Row(
    children: [
      Text(
        'Nada: 1 Rindu Untuk GIGI',
        style: GoogleFonts.montserrat(
          color: AppColors.darkBackground,
          fontWeight: FontWeight.w700,
          fontSize: 14,
        ),
      ),
    ],
  );
}

///Widget Schedule
Widget _schedule(BuildContext context) {
  return Flexible(
    child: SizedBox(
      width: SizeConfig.horizontal(95),
      height: SizeConfig.vertical(3),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: SizeConfig.horizontal(3.5)),
            child: Image.asset(
              'assets/icons/clock.png',
              width: 20,
              height: 20,
            ),
          ),
          Expanded(
            child: Text(
              'Saturday, 18 November 2022, 16.56',
              style: GoogleFonts.montserrat(
                  color: AppColors.darkBackground,
                  fontSize: 11.5,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ],
      ),
    ),
  );
}

///Widget Location
Widget _location1(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(70),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/location.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            'Black Owl PIK',
            style: GoogleFonts.montserrat(
                color: AppColors.basicDark,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    ),
  );
}

///Widget Available
Widget _available(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
        left: SizeConfig.horizontal(69), bottom: SizeConfig.vertical(0.5)),
    child: Container(
      padding: EdgeInsets.only(
          left: SizeConfig.horizontal(2), top: SizeConfig.vertical(0.7)),
      width: SizeConfig.horizontal(24),
      height: SizeConfig.vertical(3),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2), color: AppColors.basicDark),
      child: Text(
        'Available',
        style:
            GoogleFonts.montserrat(fontSize: 10, fontWeight: FontWeight.w400),
      ),
    ),
  );
}
