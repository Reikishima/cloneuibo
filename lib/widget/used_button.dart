import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// ignore: depend_on_referenced_packages
import 'package:get/get.dart';
import '../Etiket/e_ticket_qrused.dart';
import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class UsedButton extends StatelessWidget {
  const UsedButton({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
      width: SizeConfig.horizontal(95),
      height: SizeConfig.vertical(17),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(backgroundColor: AppColors.lightGold),
        onPressed: () {
          Get.to(const Etiketqrused());
        },
        child: _mainwrapper2(context),
      ),
    );
  }
}

///Widget Main Wrapper
Widget _mainwrapper2 (BuildContext context){
  return Container(
          padding: EdgeInsets.only(bottom: SizeConfig.vertical(0.5)),
          width: SizeConfig.horizontal(85),
          height: SizeConfig.vertical(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
                child: SizedBox(
                  width: SizeConfig.horizontal(95),
                  height: SizeConfig.vertical(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      _nada(context),
                      _scheduleused(context),
                      _locationused(context)
                    ],
                  ),
                ),
              ),
              _used(context),
            ],
          ),
        );
}
///Widget Nada 1 Rindu
Widget _nada(BuildContext context) {
  return Row(
    children: [
      Text(
        'Nada: 1 Rindu Untuk GIGI',
        style: GoogleFonts.montserrat(
          color: AppColors.darkBackground,
          fontWeight: FontWeight.w700,
          fontSize: 14,
        ),
      ),
    ],
  );
}

///Widget Schedule
Widget _scheduleused(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(90),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/clock.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            'Thursday, 18 November 2022, 12.00',
            style: GoogleFonts.montserrat(
                color: AppColors.darkBackground,
                fontSize: 11.5,
                fontWeight: FontWeight.w500),
          ),
        ),
      ],
    ),
  );
}

///Widget location
Widget _locationused(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(70),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/location.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            'Black Owl PIK',
            style: GoogleFonts.montserrat(
                color: AppColors.basicDark,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    ),
  );
}

///widget Used
Widget _used(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
        left: SizeConfig.horizontal(75), bottom: SizeConfig.vertical(0.5)),
    child: Container(
      padding: EdgeInsets.only(
          left: SizeConfig.horizontal(1.5), top: SizeConfig.vertical(0.7)),
      width: SizeConfig.horizontal(12),
      height: SizeConfig.vertical(3),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2), color: AppColors.basicDark),
      child: Text(
        'Used',
        style:
            GoogleFonts.montserrat(fontSize: 10, fontWeight: FontWeight.w400),
      ),
    ),
  );
}
