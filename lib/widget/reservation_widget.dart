import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class ReservationWidget extends StatelessWidget {
  const ReservationWidget({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return  Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _reservation(context),
          _codereservation(context),
          _timeevent(context),
          _profile(context),
          _call(context),
        ],
      );
  }
}

///Widget Resevation Info
Widget _reservation(BuildContext context) {
  return Container(
    padding: EdgeInsets.only(left: SizeConfig.horizontal(1)),
    child: Row(
      children: [
        Expanded(
          child: Text(
            'Reservation Info',
            style: GoogleFonts.montserrat(
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w700,
              fontSize: 14,
            ),
          ),
        ),
      ],
    ),
  );
}

///Widget Code Reservation
Widget _codereservation(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(70),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(1),
              right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/event.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            'BLKOWL686678',
            style: GoogleFonts.montserrat(
                color: AppColors.basicDark,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    ),
  );
}

///Widget Time Event
Widget _timeevent(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(85),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(1),
              right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/clock.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            'Sat, 12 Nov 2022, 16.56',
            style: GoogleFonts.montserrat(
                color: AppColors.darkBackground,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    ),
  );
}

///Widget Profile
Widget _profile(BuildContext context) {
  return SizedBox(
    height: SizeConfig.vertical(5),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(1),
              right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/profile.png',
            width: 20,
            height: 20,
          ),
        ),
        Text(
          'Juan Bruen',
          style: GoogleFonts.montserrat(
              color: AppColors.basicDark,
              fontSize: 12,
              fontWeight: FontWeight.w500),
          textAlign: TextAlign.left,
        ),
        Padding(
          padding: EdgeInsets.only(
            left: SizeConfig.horizontal(2),
          ),
          child: Container(
            width: SizeConfig.horizontal(20),
            height: SizeConfig.vertical(4),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: AppColors.basicDark),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                ImageIcon(
                  const AssetImage(
                    'assets/icons/event.png',
                  ),
                  size: 20,
                  color: AppColors.darkFlatGold,
                ),
                Text(
                  'You',
                  style: GoogleFonts.montserrat(
                      color: AppColors.darkFlatGold,
                      fontWeight: FontWeight.w500,
                      fontSize: 16),
                )
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

///Widget call
Widget _call(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(70),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(1),
              right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/call.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            '+628989xxxx',
            style: GoogleFonts.montserrat(
                color: AppColors.basicDark,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    ),
  );
}
