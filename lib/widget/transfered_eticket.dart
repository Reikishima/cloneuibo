import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class TransferedEtikect extends StatelessWidget {
  const TransferedEtikect({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.only(left: SizeConfig.horizontal(3)),
      child: Row(
        children: [
          _iconExplaied(context),
          Padding(
            padding: EdgeInsets.only(left: SizeConfig.horizontal(2.5),),
            child: _texticonexpired(context),
          )
        ],
      ),
    );
  }
}

/// Widget Icon VoucherExp
Widget _iconExplaied(BuildContext context) {
  return ImageIcon(
    const AssetImage('assets/icons/voucherexp.png'),
    color: AppColors.whiteBackground,
    size: 40,
  );
}

/// Widget Text beside Icon Success
Widget _texticonexpired(BuildContext context) {
  return RichText(
    text: TextSpan(
      text: 'E-Ticket has been succesfully\n',
      style: GoogleFonts.montserrat(
          color: AppColors.whiteBackground,
          fontWeight: FontWeight.w500,
          fontSize: 17.5),
      children: [
        TextSpan(
          text: 'transfered',
          style:
              GoogleFonts.montserrat(fontSize: 17.5, fontWeight: FontWeight.w500),
        )
      ],
    ),
  );
}