import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class HistoryButton extends StatelessWidget {
  const HistoryButton({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
            width: SizeConfig.horizontal(95),
            height: SizeConfig.vertical(5),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: AppColors.lightGold,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              onPressed: () {},
              child: Text(
                'E-Ticked History',
                style: GoogleFonts.montserrat(
                    color: AppColors.darkBackground,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
          );
  }
}