import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class ListHolderWidget extends StatelessWidget {
  const ListHolderWidget({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ListView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: SizeConfig.vertical(0.5)),
              child: Text(
                'List of E-Ticket Holder',
                style: GoogleFonts.montserrat(
                    color: AppColors.darkBackground,
                    fontWeight: FontWeight.w700,
                    fontSize: 16),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: SizeConfig.horizontal(70),
                  height: SizeConfig.vertical(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            width: SizeConfig.horizontal(8),
                            height: SizeConfig.vertical(3),
                            padding: EdgeInsets.only(
                                right: SizeConfig.horizontal(0.5)),
                            child: Text(
                              '01.',
                              style: GoogleFonts.montserrat(
                                  color: AppColors.darkBackground,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      const ImageIcon(
                        AssetImage(
                          'assets/images/logo_bo.png',
                        ),
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: Text(
                          'Juan Bruen',
                          style: GoogleFonts.montserrat(
                              color: AppColors.darkBackground,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: ImageIcon(
                          const AssetImage(
                            'assets/icons/success.png',
                          ),
                          size: 20,
                          color: AppColors.greenInfo,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            left: SizeConfig.horizontal(1.5),
                            top: SizeConfig.vertical(1)),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: AppColors.darkBackground),
                        width: SizeConfig.horizontal(10),
                        height: SizeConfig.vertical(4),
                        child: Text(
                          'You',
                          style: GoogleFonts.montserrat(
                              color: AppColors.darkFlatGold,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.close_outlined,
                      color: AppColors.basicDark,
                      size: 30,
                    )
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: SizeConfig.horizontal(80),
                  height: SizeConfig.vertical(5),
                  child: Row(
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            width: SizeConfig.horizontal(8),
                            height: SizeConfig.vertical(3),
                            padding: EdgeInsets.only(
                                right: SizeConfig.horizontal(0.5)),
                            child: Text(
                              '02.',
                              style: GoogleFonts.montserrat(
                                  color: AppColors.darkBackground,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      const ImageIcon(
                        AssetImage(
                          'assets/images/logo_bo.png',
                        ),
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: Text(
                          'Lynn VonRueden',
                          style: GoogleFonts.montserrat(
                              color: AppColors.darkBackground,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.close_outlined,
                      color: AppColors.basicDark,
                      size: 30,
                    )
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: SizeConfig.horizontal(80),
                  height: SizeConfig.vertical(5),
                  child: Row(
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            width: SizeConfig.horizontal(8),
                            height: SizeConfig.vertical(3),
                            padding: EdgeInsets.only(
                                right: SizeConfig.horizontal(0.5)),
                            child: Text(
                              '03.',
                              style: GoogleFonts.montserrat(
                                  color: AppColors.darkBackground,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      const ImageIcon(
                        AssetImage(
                          'assets/images/logo_bo.png',
                        ),
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: Text(
                          'Shane Blanda',
                          style: GoogleFonts.montserrat(
                              color: AppColors.darkBackground,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: ImageIcon(
                          const AssetImage(
                            'assets/icons/success.png',
                          ),
                          size: 20,
                          color: AppColors.greenInfo,
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.close_outlined,
                      color: AppColors.basicDark,
                      size: 30,
                    )
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: SizeConfig.horizontal(80),
                  height: SizeConfig.vertical(5),
                  child: Row(
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            width: SizeConfig.horizontal(8),
                            height: SizeConfig.vertical(3),
                            padding: EdgeInsets.only(
                                right: SizeConfig.horizontal(0.5)),
                            child: Text(
                              '04.',
                              style: GoogleFonts.montserrat(
                                  color: AppColors.darkBackground,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      const ImageIcon(
                        AssetImage(
                          'assets/images/logo_bo.png',
                        ),
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: Text(
                          'Hilda Kiehn',
                          style: GoogleFonts.montserrat(
                              color: AppColors.darkBackground,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: ImageIcon(
                          const AssetImage(
                            'assets/icons/success.png',
                          ),
                          size: 20,
                          color: AppColors.greenInfo,
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.close_outlined,
                      color: AppColors.basicDark,
                      size: 30,
                    )
                  ],
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: SizeConfig.horizontal(80),
                  height: SizeConfig.vertical(5),
                  child: Row(
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            width: SizeConfig.horizontal(8),
                            height: SizeConfig.vertical(3),
                            padding: EdgeInsets.only(
                                right: SizeConfig.horizontal(0.5)),
                            child: Text(
                              '05.',
                              style: GoogleFonts.montserrat(
                                  color: AppColors.darkBackground,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      const ImageIcon(
                        AssetImage(
                          'assets/images/logo_bo.png',
                        ),
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: Text(
                          '-',
                          style: GoogleFonts.montserrat(
                              color: AppColors.darkBackground,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: SizeConfig.horizontal(80),
                  height: SizeConfig.vertical(5),
                  child: Row(
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            width: SizeConfig.horizontal(8),
                            height: SizeConfig.vertical(3),
                            padding: EdgeInsets.only(
                                right: SizeConfig.horizontal(0.5)),
                            child: Text(
                              '06.',
                              style: GoogleFonts.montserrat(
                                  color: AppColors.darkBackground,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      const ImageIcon(
                        AssetImage(
                          'assets/images/logo_bo.png',
                        ),
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: Text(
                          '-',
                          style: GoogleFonts.montserrat(
                              color: AppColors.darkBackground,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: SizeConfig.horizontal(80),
                  height: SizeConfig.vertical(5),
                  child: Row(
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            width: SizeConfig.horizontal(8),
                            height: SizeConfig.vertical(3),
                            padding: EdgeInsets.only(
                                right: SizeConfig.horizontal(0.5)),
                            child: Text(
                              '07.',
                              style: GoogleFonts.montserrat(
                                  color: AppColors.darkBackground,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      const ImageIcon(
                        AssetImage(
                          'assets/images/logo_bo.png',
                        ),
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: Text(
                          '-',
                          style: GoogleFonts.montserrat(
                              color: AppColors.darkBackground,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: SizeConfig.horizontal(80),
                  height: SizeConfig.vertical(5),
                  child: Row(
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            width: SizeConfig.horizontal(8),
                            height: SizeConfig.vertical(3),
                            padding: EdgeInsets.only(
                                right: SizeConfig.horizontal(0.5)),
                            child: Text(
                              '08.',
                              style: GoogleFonts.montserrat(
                                  color: AppColors.darkBackground,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                      const ImageIcon(
                        AssetImage(
                          'assets/images/logo_bo.png',
                        ),
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: SizeConfig.horizontal(2),
                        ),
                        child: Text(
                          '-',
                          style: GoogleFonts.montserrat(
                              color: AppColors.darkBackground,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        )
      ],
    );
  }
}