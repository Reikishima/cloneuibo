import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class GreenInfo extends StatelessWidget {
  const GreenInfo({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
        padding: EdgeInsets.only(
          left: SizeConfig.horizontal(3),
        ),
        child: Row(
          children: [
            _iconclaim(context),
            Padding(
              padding: EdgeInsets.only(
                left: SizeConfig.horizontal(2.5),
              ),
              child: _texticonsuccess(context),
            )
          ],
        ),
      );
  }
}

/// Widget Icon Success Claim
Widget _iconclaim(BuildContext context) {
  return ImageIcon(
    const AssetImage('assets/icons/sticker-check.png'),
    color: AppColors.whiteBackground,
    size: 40,
  );
}

/// Widget Text beside Icon Success
Widget _texticonsuccess(BuildContext context) {
  return RichText(
    text: TextSpan(
      text: 'E-Ticket has been used\n',
      style: GoogleFonts.montserrat(
          color: AppColors.whiteBackground,
          fontWeight: FontWeight.w700,
          fontSize: 17.5),
      children: [
        TextSpan(
          text: 'Saturday, 12 November 2022 13.45 PM',
          style:
              GoogleFonts.montserrat(fontSize: 12, fontWeight: FontWeight.w500),
        )
      ],
    ),
  );
}