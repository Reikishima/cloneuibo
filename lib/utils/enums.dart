enum LocationPermissionType {
  servicesDisable,
  locationDenied,
  locationDisable,
  locationEnable
}

enum MemberStatus { vvip, vip, diamond, owner, ownerRelated, reguler }

enum NetworkStatus { online, offline }

enum ValidateIndentifier { validateForgotPassword, validateLoginUser }

enum ImgType { imgTypeNetwork, imgTypeFile }

enum Navigate { next, prev }

enum SnackbarType { error, success, info, custom }

enum BrightnessType { light, dark, danger, disable, optional }

enum AlignTextType { left, center, right }

// enum identifier for endpoint
enum EndPointName {
  // auth keywords
  // endpoint POST
  registerOTP,
  registerSubmit,
  preauthorize,
  authorize,
  resendOtp,
  accessToken,
  refreshToken,
  logout,
  verificationRequest,
  openBill,
  updateProfile,
  redeem,
  resetDevice,
  resetDeviceLogin,
  cancelResetDeviceLogin,
  deleteAccountOTP,
  deleteAccountSubmit,
  // endpoint GET
  memberDetail,
  memberProfile,
  memberAccountInfo,
  memberHomeAccountInfo,
  history,
  province,
  city,
  home,
  eventList,
  eventDetail,
  promoList,
  promoDetail,
  galleryList,
  galleryDetailPage,
  voucherCategory,
  voucherList,
  pointHistory,
  pointDetail,
  redeemItemList,
  redeemCategoryList,
  redeemItemDetailPage,
  notification,
  tnc,
  privacyPolicy,
  countryCode
}

enum TypeDialog {
  verifiedIdSuccess,
  redeemVerification,
  redeemSuccess,
  promoEvent,
}

enum TypeEventItem {
  isDisplayForHome,
  isDisplayForEvents,
}

enum TypeShowPriceItem {
  isDisplayForHome,
  isDisplayForPackages,
  isDisplayForRedeem,
}

enum TypePriceItem {
  isPricePoint,
  isPriceCurrency,
}

enum NavigatePriceItem {
  toFoodPackages,
  toBottlePackages,
  toRedeem,
  none,
}

enum TypeShowFlatButton {
  toNotification,
  toAnother,
}

enum TypeItemTransaction {
  isVouchers,
  isPoints,
}

enum TypeItemStatusVouchers {
  isExpired,
  isWillUse,
}

enum TypeNavigationSectionLabel {
  none,
  navigation,
}

enum TypeDetailShimmer {
  generic,
  specify,
}

enum TypeMemberForBalanceCard {
  associate,
  other,
}
